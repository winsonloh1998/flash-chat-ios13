//
//  Message.swift
//  Flash Chat iOS13
//
//  Created by Daniel Wong on 13/07/2020.
//  Copyright © 2020 Angela Yu. All rights reserved.
//

import Foundation

struct Message{
    let sender: String
    let body: String
}
