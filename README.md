# Flash Chat iOS13 (AppBrewery Course)
An iOS Messaging Application. 

## What I Have Learned
- Apply Google Firebase
- Apply Cloud Database
- Apply Firebase Authentication (Email)

## Screenshot - Homepage
![Homepage Screenshot](https://bitbucket.org/winsonloh1998/flash-chat-ios13/raw/4dd6043ecabffb9a52f4f796fbe8a931debd6ab3/Screenshot/Homepage.png)

## Screenshot - Register
![Register Screenshot](https://bitbucket.org/winsonloh1998/flash-chat-ios13/raw/4dd6043ecabffb9a52f4f796fbe8a931debd6ab3/Screenshot/Register.png)

## Screenshot - Login
![Login Screenshot](https://bitbucket.org/winsonloh1998/flash-chat-ios13/raw/4dd6043ecabffb9a52f4f796fbe8a931debd6ab3/Screenshot/Login.png)

## Screenshot - Chat Room
![Chat Room Screenshot](https://bitbucket.org/winsonloh1998/flash-chat-ios13/raw/4dd6043ecabffb9a52f4f796fbe8a931debd6ab3/Screenshot/ChatRoom.png)